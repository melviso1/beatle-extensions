// {user.before.include.begin}
#include "wx/dc.h"
#include "wx/dcclient.h"
#include "wx/settings.h"
#include "wx/bitmap.h"
#include "wx/menu.h"
#include "wx/renderer.h"
#include "wx/aui/auibook.h"
#include "wx/aui/framemanager.h"
#include "wx/aui/dockart.h"
#include <iostream>

static wxString wxAuiChopText(wxDC& dc, const wxString& text, int max_size)
{
    wxCoord x,y;

    // first check if the text fits with no problems
    dc.GetTextExtent(text, &x, &y);
    if (x <= max_size)
        return text;

    size_t i, len = text.Length();
    size_t last_good_length = 0;
    for (i = 0; i < len; ++i)
    {
        wxString s = text.Left(i);
        s += wxT("...");

        dc.GetTextExtent(s, &x, &y);
        if (x > max_size)
            break;

        last_good_length = i;
    }

    wxString ret = text.Left(last_good_length);
    ret += wxT("...");
    return ret;
}
// {user.before.include.end}

#include "bpy.wx.aui.h"

#include <assert.h>

// {user.before.class.StdToolBarArt.begin}
// {user.before.class.StdToolBarArt.end}

/**
* Constructor.
**/
StdToolBarArt::StdToolBarArt()
:	wxAuiSimpleTabArt()
,	_active_tab_text_colour(wxSystemSettings::GetColour(wxSYS_COLOUR_CAPTIONTEXT))
,	_inactive_tab_text_colour(wxSystemSettings::GetColour(wxSYS_COLOUR_INACTIVECAPTIONTEXT))
,	_active_tab_colour(*wxWHITE)
{
}



/**
* Draw a tab
**/
void StdToolBarArt::DrawTab(wxDC& dc, wxWindow* wnd, const wxAuiNotebookPage& page, const wxRect& in_rect, int close_button_state, wxRect* out_tab_rect, wxRect* out_button_rect, int* x_extent)
{
        wxCoord normal_textx, normal_texty;
        wxCoord selected_textx, selected_texty;
        wxCoord textx, texty;
    	
        // if the caption is empty, measure some temporary text
        wxString caption = page.caption;
        if (caption.empty())
            caption = wxT("Xj");
    
        dc.SetFont(m_selectedFont);
        dc.GetTextExtent(caption, &selected_textx, &selected_texty);
    
        dc.SetFont(m_normalFont);
        dc.GetTextExtent(caption, &normal_textx, &normal_texty);
    
        // figure out the size of the tab
        wxSize tab_size = GetTabSize(dc,
                                     wnd,
                                     page.caption,
                                     page.bitmap,
                                     page.active,
                                     close_button_state,
                                     x_extent);
    
        wxCoord tab_height = tab_size.y;
        wxCoord tab_width = tab_size.x;
        wxCoord tab_x = in_rect.x;
        wxCoord tab_y = in_rect.y + in_rect.height - tab_height;
    
        caption = page.caption;
    
        // select pen, brush and font for the tab to be drawn
    
        if (page.active)
        {
            dc.SetPen(wxPen(_active_tab_colour));
            dc.SetBrush(wxBrush(_active_tab_colour));
            dc.SetFont(m_selectedFont);
            textx = selected_textx;
            texty = selected_texty;
        }
        else
        {
            dc.SetPen(m_normalBkPen);
            dc.SetBrush(m_normalBkBrush);
            dc.SetFont(m_normalFont);
            textx = normal_textx;
            texty = normal_texty;
        }
    
    
        // -- draw line --
    
        wxPoint points[7];
        points[0].x = tab_x;
        points[0].y = tab_y + tab_height - 1;
        points[1].x = tab_x + tab_height - 3;
        points[1].y = tab_y + 2;
        points[2].x = tab_x + tab_height + 3;
        points[2].y = tab_y;
        points[3].x = tab_x + tab_width - 2;
        points[3].y = tab_y;
        points[4].x = tab_x + tab_width;
        points[4].y = tab_y + 2;
        points[5].x = tab_x + tab_width;
        points[5].y = tab_y + tab_height - 1;
        points[6] = points[0];
    
        dc.SetClippingRegion(in_rect);
    
        dc.DrawPolygon(WXSIZEOF(points) - 1, points);
    
        dc.SetPen(*wxGREY_PEN);
    
        //dc.DrawLines(active ? WXSIZEOF(points) - 1 : WXSIZEOF(points), points);
        dc.DrawLines(WXSIZEOF(points), points);
    
    
        int text_offset;
    
        int close_button_width = 0;
        if (close_button_state != wxAUI_BUTTON_STATE_HIDDEN)
        {
            close_button_width = m_activeCloseBmp.GetWidth();
            text_offset = tab_x + (tab_height/2) + ((tab_width-close_button_width)/2) - (textx/2);
        }
        else
        {
            text_offset = tab_x + (tab_height/3) + (tab_width/2) - (textx/2);
        }
    
        // set minimum text offset
        if (text_offset < tab_x + tab_height)
            text_offset = tab_x + tab_height;
    
        // chop text if necessary
        wxString draw_text = wxAuiChopText(dc,
                              caption,
                              tab_width - (text_offset-tab_x) - close_button_width);
    						  
    	wxColor ink_color = (page.active)? get_active_tab_text_colour(): get_inactive_tab_text_colour();
    	dc.SetTextForeground(ink_color);
    
        // draw tab text
        dc.DrawText(draw_text,
                     text_offset,
                     (tab_y + tab_height)/2 - (texty/2) + 1);
    
    
        // draw focus rectangle
        if (page.active && (wnd->FindFocus() == wnd))
        {
            wxRect focusRect(text_offset, ((tab_y + tab_height)/2 - (texty/2) + 1),
                selected_textx, selected_texty);
    
            focusRect.Inflate(2, 2);
    
            wxRendererNative::Get().DrawFocusRect(wnd, dc, focusRect, 0);
        }
    
        // draw close button if necessary
        if (close_button_state != wxAUI_BUTTON_STATE_HIDDEN)
        {
            wxBitmap bmp;
            if (page.active)
                bmp = m_activeCloseBmp;
            else
                bmp = m_disabledCloseBmp;
    
            wxRect rect(tab_x + tab_width - close_button_width - 1,
                        tab_y + (tab_height/2) - (bmp.GetHeight()/2) + 1,
                        close_button_width,
                        tab_height - 1);
            DrawButtons(dc, rect, bmp, *wxWHITE, close_button_state);
    
            *out_button_rect = rect;
        }
    
    
        *out_tab_rect = wxRect(tab_x, tab_y, tab_width, tab_height);
    
        dc.DestroyClippingRegion();
    	
    	
}


StdToolBarArt* StdToolBarArt::Clone()
{
    StdToolBarArt *clone = new StdToolBarArt();
    clone->set_active_tab_text_colour(get_active_tab_text_colour());
    clone->set_inactive_tab_text_colour(get_inactive_tab_text_colour());
    clone->set_active_tab_colour(get_active_tab_colour());
    return clone;
}


void StdToolBarArt::DrawButtons(wxDC& dc, const wxRect& _rect, const wxBitmap& bmp, const wxColour& bkcolour, int button_state)
{
        wxRect rect = _rect;
    
        if (button_state == wxAUI_BUTTON_STATE_PRESSED)
        {
            rect.x++;
            rect.y++;
        }
    
        if (button_state == wxAUI_BUTTON_STATE_HOVER ||
            button_state == wxAUI_BUTTON_STATE_PRESSED)
        {
            dc.SetBrush(wxBrush(bkcolour.ChangeLightness(120)));
            dc.SetPen(wxPen(bkcolour.ChangeLightness(75)));
    
            // draw the background behind the button
            dc.DrawRectangle(rect.x, rect.y, 15, 15);
        }
    
        // draw the button itself
        dc.DrawBitmap(bmp, rect.x, rect.y, true);
}


/**
* Assign this toolbar art to a notebook. This method is intended to break the interface from SWIG into boost.python
* \param notebook We need to do this *really* unsafe hack for getting the AuiNotebook pointer from wx.python swig
**/
void StdToolBarArt::AssignTo(boost::python::object& notebook)
{
    wxAuiNotebook* ptr = (wxAuiNotebook* )((SwigPyObject *)notebook.ptr())->ptr;
    ptr->SetArtProvider(this);
}
/**
* Destructor.
**/
StdToolBarArt::~StdToolBarArt()
{
}

// {user.after.class.StdToolBarArt.begin}
// {user.after.class.StdToolBarArt.end}



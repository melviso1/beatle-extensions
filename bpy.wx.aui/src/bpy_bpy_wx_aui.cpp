// {user.before.include.begin}
// {user.before.include.end}

#include "bpy.wx.aui.h"

// {user.before.code.begin}
#include <iostream>
// {user.before.code.end}

#include <boost/python.hpp>


namespace bpy = boost::python;
using namespace bpy;


//------------------------------------------------
//       TYPE CONVERSIONS FROM PYTHON    
//------------------------------------------------


struct converter_from_python_object_to_wxColour
{

  converter_from_python_object_to_wxColour& register_conversion()
  {
    boost::python::converter::registry::push_back(
      &converter_from_python_object_to_wxColour::convertible,
      &converter_from_python_object_to_wxColour::construct,
      boost::python::type_id<wxColour>());
    return *this;
  }
  
  static void* convertible(PyObject* object)
  {
    return object;
  }

  static void construct( PyObject* object, boost::python::converter::rvalue_from_python_stage1_data* data)
  {
      /**
We need to debug this
**/

PyObject *p_red = boost::python::expect_non_null(PyObject_GetAttrString(object,"red"));
PyObject *p_green = boost::python::expect_non_null(PyObject_GetAttrString(object,"green"));
PyObject *p_blue = boost::python::expect_non_null(PyObject_GetAttrString(object,"blue"));
PyObject *p_alpha = boost::python::expect_non_null(PyObject_GetAttrString(object,"alpha"));

unsigned char red,green,blue,alpha;

red = (unsigned char)PyLong_AsUnsignedLong(p_red);
green = (unsigned char)PyLong_AsUnsignedLong(p_green);
blue = (unsigned char)PyLong_AsUnsignedLong(p_blue);
alpha = (unsigned char)PyLong_AsUnsignedLong(p_alpha);

typedef boost::python::converter::rvalue_from_python_storage<wxColour> converter;
void* storage = ((converter*)data)->storage.bytes;
auto value=new (storage) wxColour(red, green, blue, alpha);

Py_DECREF(p_red);
Py_DECREF(p_green);
Py_DECREF(p_blue);
Py_DECREF(p_alpha);
data->convertible = storage;

  }
};

struct converter_from_python_object_to_wxAuiNotebookPtr
{

  converter_from_python_object_to_wxAuiNotebookPtr& register_conversion()
  {
    boost::python::converter::registry::push_back(
      &converter_from_python_object_to_wxAuiNotebookPtr::convertible,
      &converter_from_python_object_to_wxAuiNotebookPtr::construct,
      boost::python::type_id<wxAuiNotebookPtr>());
    return *this;
  }
  
  static void* convertible(PyObject* object)
  {
    return object;
  }

  static void construct( PyObject* object, boost::python::converter::rvalue_from_python_stage1_data* data)
  {
      
	SwigPyObject *p_swig = reinterpret_cast<SwigPyObject*>(object);

	typedef boost::python::converter::rvalue_from_python_storage<wxAuiNotebookPtr> converter;
	void* storage = ((converter*)data)->storage.bytes;
	auto value=new (storage) wxAuiNotebookPtr();

	*value=reinterpret_cast<wxAuiNotebook *>(p_swig->ptr); //copy ptr to notebook
	
	data->convertible = storage;

  }
};
//------------------------------------------------
//       DOCSTRINGS FOR CLASS wxColour
//------------------------------------------------

//------------------------------------------------
//       DOCSTRINGS FOR CLASS StdToolBarArt
//------------------------------------------------
const char* comment_class_StdToolBarArt = "Override wxAuiSimpleTabArt allowing to customize text tab colour";
const char* comment_ctor_StdToolBarArt_1 = "Constructor.";
const char* comment_StdToolBarArt_AssignTo_1 = "Assign this toolbar art to a notebook. This method is intended to break the interface from SWIG into boost.python";

BOOST_PYTHON_MODULE(bpy_wx_aui)
{
	converter_from_python_object_to_wxColour().register_conversion();
	converter_from_python_object_to_wxAuiNotebookPtr().register_conversion();
	class_<wxColour>("wxColour",no_init)
;
	class_<StdToolBarArt,boost::noncopyable>("StdToolBarArt",comment_class_StdToolBarArt,no_init)
		.def(bpy::init<>(comment_ctor_StdToolBarArt_1))
		.def("set_active_tab_text_colour", &StdToolBarArt::set_active_tab_text_colour)
		.def("set_inactive_tab_text_colour", &StdToolBarArt::set_inactive_tab_text_colour)
		.def("Clone", &StdToolBarArt::Clone,return_value_policy<reference_existing_object>())
		.def("AssignTo", &StdToolBarArt::AssignTo,comment_StdToolBarArt_AssignTo_1)
		.def("set_active_tab_colour", &StdToolBarArt::set_active_tab_colour)
;
}

// {user.after.code.begin}
// {user.after.code.end}


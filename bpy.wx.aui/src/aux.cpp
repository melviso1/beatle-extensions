// {user.before.include.begin}
// {user.before.include.end}

#include "bpy.wx.aui.h"

// {user.before.code.begin}
#include <sstream>
// {user.before.code.end}



/*
Convert Py_Object to std::string
*/
std::string to_string(PyObject* obj)
{
    std::stringstream ss;
    if (PyUnicode_Check(obj)) 
    {
        PyObject * temp_bytes = PyUnicode_AsEncodedString(obj, "UTF-8", "strict"); // Owned reference
        if (temp_bytes != NULL) {
            ss << (char *)PyBytes_AS_STRING(temp_bytes); // Borrowed pointer
            Py_DECREF(temp_bytes);
        } else {
            ss << "#!@ error conversion";
        }
    }
    else
    {
    	if (PyBytes_Check(obj)) {
    		ss << (char *)PyBytes_AS_STRING(obj); // Borrowed pointer
        }
    	else
    	{
    		PyObject *temp = PyObject_Repr(obj);
    		ss << to_string(temp);
    		Py_DECREF(temp);
    	}
    }
    return ss.str();
}

// {user.after.code.begin}
// {user.after.code.end}


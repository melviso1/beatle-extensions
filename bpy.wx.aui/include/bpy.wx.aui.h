/***

 File    :bpy.wx.aui.h
 Created :20/12/2020 15:17:26
 Author  :mel viso

 Licensed under wxWidgets license

***/

#if !defined(BPY_WX_AUI_H_INCLUDED)
#define BPY_WX_AUI_H_INCLUDED

//standard includes
/**
context wxUSE_AUI:
**/

#define wxUSE_AUI 1


//forward references
class StdToolBarArt;

#include "StdToolBarArt.h"

#define INCLUDE_INLINES
#include "StdToolBarArt.h"

#include "aux.h"

#endif //BPY_WX_AUI_H_INCLUDED


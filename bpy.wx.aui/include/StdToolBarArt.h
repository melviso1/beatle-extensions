#if !defined(STDTOOLBARART_H_INCLUDED)
#define STDTOOLBARART_H_INCLUDED


// {user.before.class StdToolBarArt.begin}
#include <wx/aui/auibook.h>
#include <boost/python.hpp>

/** This type must defined here because it 
needs wxAuiNotebook declared first **/
typedef wxAuiNotebook* wxAuiNotebookPtr;
// {user.before.class StdToolBarArt.end}


/**
* Override wxAuiSimpleTabArt allowing to customize text tab colour
**/
class StdToolBarArt : public wxAuiSimpleTabArt
{

    // {user.inside.first.class StdToolBarArt.begin}
    // {user.inside.first.class StdToolBarArt.end}

    public:
    StdToolBarArt();
    void DrawTab(wxDC& dc, wxWindow* wnd, const wxAuiNotebookPage& page, const wxRect& in_rect, int close_button_state, wxRect* out_tab_rect, wxRect* out_button_rect, int* x_extent);
    StdToolBarArt* Clone();
    static void DrawButtons(wxDC& dc, const wxRect& _rect, const wxBitmap& bmp, const wxColour& bkcolour, int button_state);
    void AssignTo(boost::python::object& notebook);
    virtual ~StdToolBarArt();
    private:
    wxColour _active_tab_text_colour ;
    wxColour _inactive_tab_text_colour ;
    /**
    * The colour used for active tab.
    **/
    wxColour _active_tab_colour ;
    public:
    inline void set_active_tab_text_colour(const wxColour& ref_active_tab_text_colour);
    inline const wxColour& get_active_tab_text_colour() const;
    inline void set_inactive_tab_text_colour(const wxColour& ref_inactive_tab_text_colour);
    inline const wxColour& get_inactive_tab_text_colour() const;
    inline void set_active_tab_colour(const wxColour& ref_active_tab_colour);
    inline const wxColour& get_active_tab_colour() const;
    // {user.inside.last.class StdToolBarArt.begin}
    // {user.inside.last.class StdToolBarArt.end}
};

// {user.after.class StdToolBarArt.begin}
// {user.after.class StdToolBarArt.end}

#endif //STDTOOLBARART_H_INCLUDED

#if defined(INCLUDE_INLINES)
#if !defined(INCLUDE_INLINES_STDTOOLBARART)
#define INCLUDE_INLINES_STDTOOLBARART



inline void StdToolBarArt::set_active_tab_text_colour(const wxColour& ref_active_tab_text_colour)
{
    _active_tab_text_colour=ref_active_tab_text_colour;
}


inline const wxColour& StdToolBarArt::get_active_tab_text_colour() const
{
    return _active_tab_text_colour;
}


inline void StdToolBarArt::set_inactive_tab_text_colour(const wxColour& ref_inactive_tab_text_colour)
{
    _inactive_tab_text_colour=ref_inactive_tab_text_colour;
}


inline const wxColour& StdToolBarArt::get_inactive_tab_text_colour() const
{
    return _inactive_tab_text_colour;
}


inline void StdToolBarArt::set_active_tab_colour(const wxColour& ref_active_tab_colour)
{
    _active_tab_colour=ref_active_tab_colour;
}


inline const wxColour& StdToolBarArt::get_active_tab_colour() const
{
    return _active_tab_colour;
}

#endif //(INCLUDE_INLINES_STDTOOLBARART)
#endif //INCLUDE_INLINES



#if !defined(AUX_H_INCLUDED)
#define AUX_H_INCLUDED
// {{user.before.declarations.begin}}

#define PY_SSIZE_T_CLEAN
#include <Python.h>

typedef unsigned long swig_type_info;

typedef struct {
    PyObject_HEAD
    void *ptr;
    swig_type_info *ty;
    int own;
    PyObject *next;
    PyObject *dict;
} SwigPyObject;
// {{user.before.declarations.end}}


//non template methods
std::string  to_string(PyObject* obj);

// {{user.after.declarations.begin}}
// {{user.after.declarations.end}}

#endif //AUX_H_INCLUDED


